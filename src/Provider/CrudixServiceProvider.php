<?php

namespace Crudix\Provider;

use Config;
use Package;
use Concrete\Core\Foundation\Service\Provider as ServiceProvider;

class CrudixServiceProvider extends ServiceProvider
{
    /**
     * Registers the services provided by this provider.
     *
     * @return void
     */
    public function register()
    {
        \Events::addListener('on_before_dispatch', [$this, 'registerRoutes']);
    }

    /**
     * Finds the crudix configs and registers the
     * necessary routes.
     *
     * @return void
     */
    public function registerRoutes()
    {
        $controllers = Config::get('app.crudix', []);
        $packages = Package::getInstalledHandles();

        // get all the controllers from all packages
        foreach ($packages as $handle) {
            $controllers = array_merge($controllers, Config::get(sprintf('%s::app.crudix', $handle), []));
        }

        // register the routes
        foreach ($controllers as $controller) {
            if (method_exists($controller, 'registerRoutes')) {
                call_user_func([$controller, 'registerRoutes']);
            }
        }
    }
}
