<?php

namespace Crudix;

use Illuminate\Database\Eloquent\Builder;

class Paginator
{
    /**
     * The base eloquent builder instance.
     *
     * @var \Illuminate\Database\Eloquent\Builder
     */
    protected $select;

    /**
     * The current sticky request object.
     *
     * @var \Concrete\Core\Search\StickyRequest;
     */
    protected $sticky;

    /**
     * The default number of items per page.
     *
     * @var integer
     */
    protected $itemsPerPage = 10;

    /**
     * The default number pages in pagination bar.
     *
     * @var integer
     */
    protected $pagesDisplayed = 10;

    /**
     * Paginator constructor.
     *
     * @param $select \Illuminate\Database\Eloquent\Builder
     * @param $sticky \Concrete\Core\Search\StickyRequest;
     */
    public function __construct(Builder $select, $sticky)
    {
        $this->select = $select;
        $this->sticky = $sticky;
    }

    /**
     * Set the paginator params.
     *
     * @param int $itemsPerPage
     * @param int $pagesDisplayed
     */
    public function setProps($itemsPerPage, $pagesDisplayed)
    {
        $this->itemsPerPage = $itemsPerPage;
        $this->pagesDisplayed = $pagesDisplayed;
    }

    /**
     * Build pagination based on request.
     *
     * @return array;
     */
    public function build()
    {
        $offset = $this->calculatePaginationOffset();

        $this->select->take($this->itemsPerPage)->skip($offset);

        return $this->paginateResponse();
    }

    /**
     * Checks if has enough records for pagination.
     *
     * @return bool
     */
    public function hasPagination()
    {
        return $this->itemsPerPage && ($this->getRecordsNumber() > $this->itemsPerPage);
    }

    /**
     * Get page offset.
     *
     * @return int
     */
    protected function calculatePaginationOffset()
    {
        return ($this->getCurrentPage() - 1) * $this->itemsPerPage;
    }

    /**
     * Get current page.
     *
     * @return int
     */
    public function getCurrentPage()
    {
        $request = $this->sticky->getSearchRequest();
        $pages = $this->getPageNumber();

        if (isset($request['page']) && $this->in($request['page'], 1, $pages)) {
            return $request['page'];
        }

        return 1;
    }

    /**
     * Gets number of pages.
     *
     * @return int
     */
    public function getPageNumber()
    {
        $count = $this->getRecordsNumber() ?: 1;

        return ceil($count / $this->itemsPerPage);
    }

    /**
     * Returns number of records in current select.
     *
     * @return int
     */
    public function getRecordsNumber()
    {
        $select = clone $this->select;

        $select->getQuery()->limit = null;
        $select->getQuery()->offset = null;

        return $select->count();
    }

    /**
     * Creates the pagination response object.
     *
     * @return array
     */
    public function paginateResponse()
    {
        $pagination = new \stdClass();
        $pagination->pages = [];

        $pages = $this->getPageNumber();
        $currentPage = $this->getCurrentPage();
        $range = $this->getDisplayedPagesRange($currentPage, $pages);

        for ($i = $range->start; $i <= $range->end; $i++) {
            array_push($pagination->pages, $i);
        }

        $pagination->page = $currentPage;
        $pagination->prev = ($currentPage - 1) ?: 0;
        $pagination->next = ($currentPage + 1 <= $pages) ? $currentPage + 1 : 0;

        return json_decode(json_encode($pagination), true);
    }

    /**
     * Get page range which gets to be displayed.
     *
     * @param int $currentPage
     * @return \stdClass
     */
    protected function getDisplayedPagesRange($currentPage, $pages)
    {
        $range = new \stdClass();

        $availablePages = $this->pagesDisplayed - 1;

        $range->start = $currentPage - floor($availablePages / 2);
        $range->end = $currentPage + ceil($availablePages / 2);

        if ($range->end > $pages) {
            $range->start -= $range->end - $pages;
        }

        if ($range->start < 1) {
            $range->end += abs($range->start);
            $range->start = 1;
        }

        if ($range->end > $pages) {
            $range->end = $pages;
        }

        return $range;
    }

    /**
     * Check if value is in interval.
     *
     * @param $value
     * @param $min
     * @param $max
     * @return bool
     */
    private function in($value, $min, $max)
    {
        return ($value >= $min && $value <= $max);
    }
}
