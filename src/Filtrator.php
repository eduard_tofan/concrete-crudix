<?php

namespace Crudix;

use Illuminate\Database\Eloquent\Builder;

class Filtrator
{
    /**
     * The base eloquent builder instance.
     *
     * @var \Illuminate\Database\Eloquent\Builder
     */
    protected $select;

    /**
     * The current filter setup.
     *
     * @var \Crudix\Filter []
     */
    protected $filters = [];

    /**
     * The current sticky request object.
     *
     * @var \Concrete\Core\Search\StickyRequest;
     */
    protected $sticky;

    /**
     * Types of filters which are compared with equals operator.
     *
     * @var array
     */
    protected $fixedTypes = ['select', 'selectMultiple', 'text', 'number'];

    /**
     * Sequencer constructor.
     *
     * @param $select \Illuminate\Database\Eloquent\Builder
     * @param $sticky \Concrete\Core\Search\StickyRequest;
     */
    public function __construct(Builder $select, $sticky)
    {
        $this->select = $select;
        $this->sticky = $sticky;
    }

    /**
     * Set the filtrator params.
     *
     * @param \Crudix\Filter [] $filters
     */
    public function setProps($filters)
    {
        $this->filters = $filters;
    }

    /**
     * Build filters based on request.
     */
    public function build()
    {
        if ($this->filters) {
            $this->applyFilters();
        }

        return $this->buildAdvancedFilters();
    }

    /**
     * Applies the columns filtering.
     */
    protected function applyFilters()
    {
        $this->setFilterValues();

        foreach($this->filters as $filter) {
            if ($filter->isActive()) {
                $this->addFilter($filter);
            }
        }
    }

    /**
     * Sets filter values from request.
     */
    protected function setFilterValues()
    {
        $request = $this->sticky->getSearchRequest();

        foreach ($this->filters as $filter) {
            if ($this->hasValue($request, $filter->name)) {
                $filter->setValue($request[$filter->name]);
            }
        }
    }

    /**
     * Build the persistent advanced filters.
     */
    protected function buildAdvancedFilters()
    {
        $fields = [];

        foreach ($this->filters as $filter) {
            if ($filter->isAdvanced() && $filter->isActive()) {
                array_push($fields, $filter->field());
            }
        }

        return $fields;
    }

    /**
     * Adds the fields of the filter to the builder.
     *
     * @param \Crudix\Filter $filter
     */
    protected function addFilter($filter)
    {
        if ($filter->hasClosure()) {
            $filter->runClosure($this->select, $filter->value, $this->sticky->getSearchRequest());
        }
        else if(in_array($filter->type, $this->fixedTypes)) {
            $this->handleFixedTypes($filter->fields, $filter->value);
        } else {
            $this->handleSearchType($filter->fields, $filter->value);
        }
    }

    /**
     * Build fixed type query with equals operator.
     *
     * @param array $fields
     * @param array|string $value
     */
    protected function handleFixedTypes($fields, $value)
    {
        foreach ($fields as $field) {
            if (is_array($value)) {
                $this->select->whereIn($field, $value);
            } else {
                $this->select->where($field, $value);
            }
        }
    }

    /**
     * Builds search type query with like operator.
     *
     * @param array $fields
     * @param string $value
     */
    protected function handleSearchType($fields, $value)
    {
        $parts = explode(' ', $value);

        if (! count($parts)) {
            return;
        };

        // implode all columns for easy searching
        $field = 'concat(' . implode(',', $fields) . ')';

        $this->select->where(function ($query) use ($parts, $field) {
            foreach ($parts as $part) {
                $query->whereRaw($field . ' like ?', ["%$part%"], 'or');
            }
        });
    }

    /**
     * Get a filter as field.
     *
     * @param string $name
     * @return array
     */
    public function field($name)
    {
        $field = $this->has($name);

        return $field ? $field->field() : [];
    }

    /**
     * Gets a filter by name.
     *
     * @param $name
     * @return \Crudix\Filter|null
     */
    public function has($name)
    {
        foreach ($this->filters as $filter) {
            if ($filter->name == $name) {
                return $filter;
            }
        }

        return null;
    }

    /**
     * Builds and returns the filters in response format.
     */
    public function getResponseFilters()
    {
        $results = [];

        foreach ($this->filters as $filter) {
            $results[$filter->getCategory()][] = $filter->toArray();
        }

        return $results;
    }

    /**
     * Checks if a key exists and has data.
     *
     * @param array $filter
     * @param string $key
     * @return bool|int
     */
    protected function hasValue($filter, $key)
    {
        if (! isset($filter[$key])) return false;

        if (is_array($filter[$key])) {
            return count($filter[$key]);
        }

        return strlen($filter[$key]);
    }
}
