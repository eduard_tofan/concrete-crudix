<?php

namespace Crudix\Api;

use URL;
use Crudix\Engine;
use Crudix\Support\Str;
use Concrete\Core\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse as Response;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;

abstract class Controller extends AbstractController
{
    /**
     * The associated crud engine object.
     *
     * @var \Crudix\Engine
     */
    protected $engine;

    /**
     * The request object.
     *
     * @var \Symfony\Component\HttpFoundation\Request
     */
    protected $request;

    /**
     * The dynamic parameters which are injected into the request object.
     *
     * @var array
     */
    protected $dynamicParameters;

    /**
     * The base path from which all routes and urls will be made.
     *
     * @var string
     */
    protected $baseRoute = '/dashboard/some_page';

    /**
     * The edit/add url.
     *
     * @var string
     */
    protected $addUrl = '';

    /**
     * Reset engine params on refresh.
     *
     * @var bool
     */
    protected $resetOnRefresh = true;

    /**
     * The registered bulk actions.
     *
     * @var array
     */
    protected $bulkActions = ['delete'];

    /**
     * The name of the csrf token.
     *
     * @var string
     */
    protected $csrf = 'crudix_csrf_form_token';

    /**
     * All available routes.
     *
     * @var array
     */
    protected $routes = ['search', 'page', 'sort', 'field', 'bulk'];

    /**
     * Controller constructor.
     *
     * @param array $request
     */
    public function __construct($request = [])
    {
        parent::__construct();

        $this->handleDynamicParameters($request);

        $this->setEngine($this->getEngine());
        $this->engine->setController($this);
    }

    /**
     * Set up the crud engine.
     *
     * @return \Crudix\Engine $engine
     */
    protected function getEngine()
    {
        // override and return the crud engine model.
    }

    /**
     * Get the base url.
     *
     * @param string $trail
     * @return string
     */
    public function getRouteUrl($trail = '')
    {
        return rtrim(URL::to("{$this->baseRoute}$trail")->__toString(), '/');
    }

    /**
     * Get the add/edit url.
     *
     * @return string
     */
    protected function getAddUrlString()
    {
        return $this->addUrl ?: $this->baseRoute . '/edit';
    }

    /**
     * Get the add/edit url.
     *
     * @param string $trail
     * @return string
     */
    public function getAddUrl($trail = '')
    {
        return rtrim(URL::to($this->getAddUrlString() . $trail)->__toString(), '/');
    }

    /**
     * Get the add button title.
     *
     * @return string
     */
    protected function getAddUrlTitle()
    {
        return t('Add new');
    }

    /**
     * Get the default bulk action dialog title.
     *
     * @return string
     */
    public function getBulkActionTitle()
    {
        return t('Are you sure ?');
    }

    /**
     * Get the default bulk action dialog message.
     *
     * @return string
     */
    public function getBulkActionMessage()
    {
        return t('You are going to {action} {count} items.');
    }

    /**
     * Gets the bulk actions mapping.
     * Available keys {alias|title|message}
     *
     * @return array
     */
    public function getBulkActionMapping()
    {
        return [];
    }

    /**
     * Getter for bulk actions array.
     *
     * @return array
     */
    public function getBulkActions()
    {
        return $this->bulkActions;
    }

    /**
     * Check if bulk action exists.
     *
     * @param string $action
     * @return bool
     */
    public function hasBulkAction($action)
    {
        return in_array($action, $this->bulkActions);
    }

    /**
     * Get the crud response.
     *
     * @return array
     */
    public function response()
    {
        if ($this->resetOnRefresh) {
            $this->engine->resetSearchRequest();
        }

        return $this->engine->run()->toArray();
    }

    /**
     * Get routes array.
     *
     * @return array
     */
    public function getRoutes()
    {
        return $this->routes;
    }

    /**
     * Get the crud filters.
     *
     * @return array
     */
    public function getFilters()
    {
        return $this->engine->getFilters();
    }

    /**
     * Search default route method.
     */
    public function search()
    {
        $this->engine->resetSearchRequest();
        $this->engine->setFilters($_REQUEST);

        return new Response($this->engine->run()->toArray());
    }

    /**
     * Pagination default route method.
     *
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function paginate($page)
    {
        $this->engine->setPage($page);

        return new Response($this->engine->run()->toArray());
    }

    /**
     * Sorting default route method.
     *
     * @param string $by
     * @param string $order {asc|desc}
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function sort($by, $order)
    {
        $this->engine->setOrder($by, $order);

        return new Response($this->engine->run()->toArray());
    }

    /**
     * Filter field default route method.
     *
     * @param string $field
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function field($field)
    {
        return new Response($this->engine->getFilterField($field));
    }

    /**
     * Deletes selected entries.
     *
     * @param array $items
     */
    public function bulkDeleteAction($items)
    {
        $model = $this->engine->getModel()->getModel();

        $model::destroy($items);
    }

    /**
     * Bulk action route entry point.
     *
     * @param string $action
     * @return mixed
     */
    public function bulk($action)
    {
        if (! $this->hasBulkAction($action)) {
            throw new InvalidArgumentException('Action ' . $action . ' doesn\'t exist.');
        }

        $method = $this->getBulkMethod($action);
        $items = $this->request('item') ?: [];

        return call_user_func([$this, $method], array_values($items));
    }

    /**
     * Register the search route.
     */
    protected function registerSearchRoute()
    {
        \Route::register($this->baseRoute . '/search', get_class($this) . '::search');
    }

    /**
     * Register the pagination route.
     */
    protected function registerPageRoute()
    {
        \Route::register($this->baseRoute . '/page/{page}', get_class($this) . '::paginate');
    }

    /**
     * Register the sort route.
     */
    protected function registerSortRoute()
    {
        \Route::register($this->baseRoute . '/sort/{by}/{order}', get_class($this) . '::sort');
    }

     /**
     * Register the field route.
     */
    protected function registerFieldRoute()
    {
        \Route::register($this->baseRoute . '/field/{field}', get_class($this) . '::field');
    }

     /**
     * Register the search route.
     */
    protected function registerBulkRoute()
    {
        \Route::register($this->baseRoute . '/bulk/{action}', get_class($this) . '::bulk');
    }

    /**
     * Registers all default routes.
     */
    public static function registerRoutes()
    {
        $instance = new static;

        foreach ($instance->getRoutes() as $route) {
            call_user_func([$instance, $instance->getRegisterRouteMethod($route)]);
        }
    }

    /**
     * Runs a method mapped to a route.
     *
     * @param string $action
     * @param array $parameters
     * @return mixed
     */
    public function runAction($action, $parameters = array())
    {
        $response = Middleware::run($this->baseRoute, $this->csrf);

        return $response ?: parent::runAction($action, $parameters);
    }

    /**
     * Handles dynamic request params.
     *
     * @param array $request
     */
    public function handleDynamicParameters($request)
    {
        if ($extra = $this->request('crudix_dynamic_request_parameters')) {
            $request = array_merge($extra, $request);
        }

        $this->dynamicParameters = $request;

        $this->addToRequest($request);
    }

    /**
     * Return null as, this is an api controller.
     *
     * @return null
     */
    public function getViewObject()
    {
        return null;
    }

    /**
     * Builds and gets the view params.
     *
     * @param null $namespace
     * @return array
     */
    public function getView($namespace = null)
    {
        if ($namespace) {
            $this->engine->setNamespace($namespace);
        }

        $urls = $this->getViewUrls();

        $misc = [
            'searchResult' => $this->response(),
            'dataSearch' => $this->getDataSearch(),
            'addTitle' => $this->getAddUrlTitle(),
            'bulkActions' => $this->getAllBulkActions(),
            'filters' => $this->getFilters(),
            'dynamicParameters' => $this->dynamicParameters,
            'csrf' => $this->csrf
        ];

        return array_merge($misc, $urls);
    }

    /**
     * Gets the controller instance, and injects the extra params.
     *
     * @param array $request
     * @return static
     */
    public static function instance($request = [])
    {
        return new static($request);
    }

    /**
     * Add params to the controller request.
     *
     * @param array $params
     */
    protected function addToRequest($params)
    {
        if ($params) {
            $this->request->request->add($params);
        }
    }

    /**
     * Gets the register route method or throws exception.
     *
     * @param string $route
     * @return string
     *
     * @throws InvalidArgumentException
     */
    protected function getRegisterRouteMethod($route)
    {
        $methods = get_class_methods($this);

        foreach ($methods as $method) {
            if ($this->isRegisterRouteMethod($method, $route)) {
                return $method;
            }
        }

        throw new InvalidArgumentException('Controller route register method for route ' . $route . ' not found!');
    }

    /**
     * Gets the bulk action method or throws exception.
     *
     * @param $action
     * @return string
     *
     * @throws InvalidArgumentException
     */
    protected function getBulkMethod($action)
    {
        $methods = get_class_methods($this);

        foreach ($methods as $method) {
            if ($this->isBulkActionMethod($method, $action)) {
                return $method;
            }
        }

        throw new InvalidArgumentException('Controller bulk action method for action ' . $action . ' not found!');
    }

    /**
     * Check if class function is in bulk action format.
     *
     * @param string $method
     * @param string $action
     * @return bool
     */
    protected function isBulkActionMethod($method, $action)
    {
        return starts_with($method, 'bulk') && ends_with($method, 'Action')
        && str_contains(strtolower($method), strtolower(camel_case($action)));
    }

    /**
     * Check if class function is in register route format.
     *
     * @param string $method
     * @param string $route
     * @return bool
     */
    protected function isRegisterRouteMethod($method, $route)
    {
        return starts_with($method, 'register') && ends_with($method, 'Route')
        && str_contains(strtolower($method), strtolower(camel_case($route)));
    }

    /**
     * Sets the crud engine.
     *
     * @param \Crudix\Engine $engine
     */
    protected function setEngine(Engine $engine)
    {
        $this->engine = $engine;
    }

    /**
     * Get all attributes for all bulk action.
     *
     * @return array
     */
    protected function getAllBulkActions()
    {
        $actions = [];

        foreach ($this->getBulkActions() as $action) {
            array_push($actions, $this->getActionAttributes($action));
        }

        return $actions;
    }

    /**
     * Get all attributes for a bulk action.
     *
     * @param string $action
     * @return array
     */
    protected function getActionAttributes($action)
    {
        $props = $this->getActionProperties($action);

        return [
            'attributes' => [
                'data-bulk-action-type' => 'dialog',
                'data-bulk-action-name' => $action,
                'data-bulk-action-title' => $props['title'],
                'data-bulk-action-message' => $props['message'],
                'data-bulk-action-url' => $this->getRouteUrl('/bulk/' . $action)
            ],
            'title' => $props['alias'],
        ];
    }

    /**
     * Get the pop-up attributes for a bulk action.
     *
     * @param string $action
     * @return array
     */
    protected function getActionProperties($action)
    {
        $props = [];

        $mapping = $this->getBulkActionMapping();
        $hasAction = isset($mapping[$action]) ? $mapping[$action] : null;

        $props['title'] = isset($hasAction['title']) ? $hasAction['title'] : $this->getBulkActionTitle();
        $props['message'] = isset($hasAction['message']) ? $hasAction['message'] : $this->getBulkActionMessage();
        $props['alias'] = isset($hasAction['alias']) ? $hasAction['alias'] : Str::readable($action);

        return $props;
    }

    /**
     * Get the view url configuration.
     *
     * @return array
     */
    protected function getViewUrls()
    {
        return [
            'addUrl' => $this->getAddUrl(),
            'searchUrl' => $this->getRouteUrl('/search'),
            'sortingUrl' => $this->getRouteUrl('/sort'),
            'paginationUrl' => $this->getRouteUrl('/page'),
            'fieldsUrl' => $this->getRouteUrl('/field')
        ];
    }

    /**
     * Get the namespace/unique identifier.
     *
     * @return string
     */
    protected function getDataSearch()
    {
        $namespace = explode('\\', $this->engine->getNamespace());

        return count($namespace) ? $namespace[count($namespace) - 1] : (string) rand();
    }

    public function request($key = null)
    {
        if (is_null($key)) {
            return array_merge(
                $this->request->request->all(),
                $this->request->attributes->all(),
                $this->request->query->all()
            );
        }

        return $this->request->get($key);
    }
}
