<?php

namespace Crudix\Api;

use \Page;
use \Permissions;
use Concrete\Core\Validation\CSRF\Token;
use Symfony\Component\HttpFoundation\Response;

class Middleware
{
    /**
     * Checks if path can be accessed.
     *
     * @param string $path
     * @return \Symfony\Component\HttpFoundation\Response|null
     */
    public function handle($path)
    {
        $page = Page::getByPath($path);

        $permissions = new Permissions($page);

        if(! $permissions->canViewPage()) {
            return $this->deny();
        }

        return null;
    }

    /**
     * Checks csrf token is valid.
     *
     * @param string $csrf
     * @return \Symfony\Component\HttpFoundation\Response|null
     */
    public function csrf($csrf)
    {
        if(! (new Token)->validate($csrf)) {
            return $this->deny(t('Invalid CSRF token.'));
        }

        return null;
    }

    /**
     * Deny access to resource.
     *
     * @param string $message
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deny($message = '')
    {
        $message = $message ?: t('Access Denied.');

        return new Response($message, 403);
    }

    /**
     * Runs the route middleware.
     *
     * @param string $path
     * @param string $csrf
     * @return \Symfony\Component\HttpFoundation\Response|null
     */
    public static function run($path, $csrf)
    {
        $instance = new static;

        return $instance->csrf($csrf) ?: $instance->handle($path);
    }
}
