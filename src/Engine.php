<?php

namespace Crudix;

use Crudix\Support\Str;
use Concrete\Core\Search\StickyRequest;
use Illuminate\Database\Capsule\Manager as Capsule;

abstract class Engine
{
    /**
     * The base eloquent builder instance.
     *
     * @var \Illuminate\Database\Eloquent\Builder
     */
    private $select;

    /**
     * The current sorting setup.
     *
     * @var \Crudix\Filtrator
     */
    private $filtrator;

    /**
     * The current sorting setup.
     *
     * @var \Crudix\Sequencer
     */
    private $sorting;

    /**
     * The base paginator instance.
     *
     * @var \Crudix\Paginator
     */
    private $pagination;

    /**
     * The current sticky request object.
     *
     * @var \Concrete\Core\Search\StickyRequest;
     */
    private $sticky;

    /**
     * The search response instance.
     *
     * @var \Crudix\Response
     */
    private $response;

    /**
     * The api controller instance.
     *
     * @var \Crudix\Api\Controller
     */
    private $controller;

    /**
     * The current selected columns.
     *
     * @var array
     */
    protected $columns = [];

    /**
     * The current relation selected columns.
     *
     * @var array
     */
    protected $relationColumns = [];

    /**
     * The current pseudo-columns.
     *
     * @var array
     */
    protected $pseudoColumns = ['edit'];

    /**
     * The sticky request namespace.
     * If let unset will be set with current namespace.
     *
     * @var string
     */
    protected $namespace = '';

    /**
     * The default number of items per page.
     *
     * @var integer
     */
    protected $itemsPerPage = 10;

    /**
     * The default number pages in pagination bar.
     *
     * @var integer
     */
    protected $pagesDisplayed = 10;

    /**
     * The default sorting order.
     *
     * @var string
     */
    protected $defaultOrder = 'asc';

    /**
     * The default sort column.
     * If left unset will sort after first sortable column.
     *
     * @var string
     */
    protected $defaultSortColumn = '';

    /**
     * The applied filters.
     *
     * @var array
     */
    protected $filters = ['keyword'];

    /**
     * The default search filter column.
     * If left unset will filter after default sort column.
     *
     * @var string
     */
    protected $defaultFilterColumns = [];

    /**
     * The model columns which are sortable.
     *
     * @var array
     */
    protected $sortableColumns = ['*'];

    /**
     * Crud Engine constructor.
     */
    public function __construct()
    {
        $this->select = $this->getModel();
        $this->sticky = new StickyRequest($this->getNamespace());

        $this->boot();
    }

    /**
     * Set the crud table Eloquent select.
     *
     * @return \Illuminate\Database\Eloquent\Builder;
     */
    public function getModel()
    {
        // override and return the table model.
    }

    /**
     * Get the crud table Eloquent select.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getSelect()
    {
        return $this->select;
    }

    /**
     * Get the sticky request.
     *
     * @return array
     */
    public function getRequest()
    {
        return $this->sticky->getSearchRequest();
    }

    /**
     * Gets the column mapping.
     *
     * @return array
     */
    public function getColumnMapping()
    {
        return [];
    }

    /**
     * Gets the pseudo-column mapping.
     *
     * @return array
     */
    public function getPseudoColumnMapping()
    {
        return [];
    }

    /**
     * Columns getter.
     *
     * @return array
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * Relation columns getter.
     *
     * @return array
     */
    public function getRelationColumns()
    {
        return $this->relationColumns;
    }

    /**
     * Pseudo columns getter.
     *
     * @return array
     */
    public function getPseudoColumns()
    {
        return $this->pseudoColumns;
    }

    /**
     * Bonus name column wrapper.
     *
     * @param string $value
     * @param \Illuminate\Database\Eloquent\Model $model
     * @return string
     */
    public function getEditWrapper($value, $model)
    {
        $url = $this->controller->getAddUrl('/' . $model->getKey());

        return '<a href="' . $url . '">' . t('Edit') . '</a>';
    }

    /**
     * Get the response filter parameters.
     *
     * @return array
     */
    public function getFilters()
    {
        return $this->filtrator->getResponseFilters();
    }

    /**
     * Get filter response for advanced filters.
     *
     * @param string $name
     * @return array
     */
    public function getFilterField($name)
    {
        $this->filtrator->build();

        if ($this->filtrator->has($name)) {
            return $this->filtrator->field($name);
        }

        return [];
    }

    /**
     * Returns the search response object.
     *
     * @return \Crudix\Response
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * Gets the sticky request namespace.
     *
     * @return string
     */
    public function getNamespace()
    {
        return $this->namespace ? $this->namespace : get_class($this->select->getModel());
    }

    /**
     * Set the namespace.
     *
     * @param $namespace
     */
    public function setNamespace($namespace)
    {
        $this->namespace = $namespace;
    }

    /**
     * Sets the controller instance.
     *
     * @param $controller
     */
    public function setController($controller)
    {
        $this->controller = $controller;
    }

    /**
     * Set the filter values parameters.
     *
     * @param array $filters
     */
    public function setFilters($filters)
    {
        foreach ($filters as $name => $value) {
            if ($this->filtrator->has($name)) {
                $this->sticky->addToSearchRequest($name, $value);
            }
        }
    }

    /**
     * Set the ordering parameters.
     *
     * @param string $column
     * @param string $direction {asc\desc}
     */
    public function setOrder($column, $direction) {
        if ($this->sorting) {
            $this->sticky->addToSearchRequest('orderColumn', $column);
            $this->sticky->addToSearchRequest('direction', $direction);
        }
    }

    /**
     * Sets the current page.
     *
     * @param int $page
     */
    public function setPage($page)
    {
        if ($this->pagination->hasPagination()) {
            $this->sticky->addToSearchRequest('page', $page);
        }
    }

    /**
     * Returns the response object.
     *
     * @return \Crudix\Response
     */
    public function run()
    {
        $this->prepareEngine();

        return $this->response;
    }

    /**
     * Resets the current request.
     */
    public function resetSearchRequest()
    {
        $this->sticky->resetSearchRequest();
    }

    /**
     * Set the items per page param.
     *
     * @param int $itemsPerPage
     * @return Engine
     */
    public function setItemsPerPage($itemsPerPage)
    {
        $this->itemsPerPage = $itemsPerPage;
        $this->pagination->setProps($this->itemsPerPage, $this->pagesDisplayed);

        return $this;
    }

    /**
     * Set the pages displayed param.
     *
     * @param int $pagesDisplayed
     * @return Engine
     */
    public function setPagesDisplayed($pagesDisplayed)
    {
        $this->pagesDisplayed = $pagesDisplayed;
        $this->pagination->setProps($this->itemsPerPage, $this->pagesDisplayed);

        return $this;
    }

    /**
     * Get the search filter.
     *
     * @return \Crudix\Filter
     */
    protected function getKeywordFilter()
    {
        return $this->filter('search')
            ->advanced(false)
            ->fields($this->defaultFilterColumns ?: [$this->sorting->getDefaultOrderColumn()])
            ->miscFields(['placeholder' => t('Search')]);
    }

    /**
     * Boot the engine.
     */
    protected function boot()
    {
        $this->initColumns();

        $this->addPrimaryKey();

        $this->initSequencer();
        $this->initFiltrator();
        $this->initPaginator();

        $this->response = new Response($this);
    }

    /**
     * Init the sequencer component.
     */
    protected function initSequencer()
    {
        $this->sorting = new Sequencer($this);

        $this->sorting->setProps(
            $this->defaultOrder, $this->defaultSortColumn,
            $this->sortableColumns, $this->getColumnMapping()
        );
    }

    /**
     * Init the filtrator component.
     */
    protected function initFiltrator()
    {
        $this->filtrator = new Filtrator($this->select, $this->sticky);
        $this->filtrator->setProps($this->getSetFilters());
    }

    /**
     * Init the paginator component.
     */
    protected function initPaginator()
    {
        $this->pagination = new Paginator($this->select, $this->sticky);
        $this->pagination->setProps($this->itemsPerPage, $this->pagesDisplayed);
    }

    /**
     * Builds the query and engine data.
     */
    protected function prepareEngine()
    {
        $this->response->setOrder($this->sorting->build());
        $this->response->setFilters($this->filtrator->build());

        if ($this->pagination->hasPagination()) {
            $this->response->setPagination($this->pagination->build());
        }

        $this->response->build($this->select->get());
    }

    /**
     * Gets a new filter instance.
     *
     * @param string $type
     * @return \Crudix\Filter
     */
    protected function filter($type)
    {
        return new Filter(['type' => $type]);
    }

    /**
     * Get the selected columns from the @select object.
     */
    private function initColumns()
    {
        $all = $this->getTableColumns();
        $selected = $this->select->getQuery()->columns;

        $columns = $this->columns ?: (in_array('*', $selected) ? $all : $selected);

        $this->fillColumns($columns, $all);
    }

    /**
     * Get all table columns.
     *
     * @return array
     */
    public function getTableColumns()
    {
        $table = $this->select->getQuery()->from;

        // here we get the doctrine schema manager
        // to get all column names even if the table is empty
        $conn = Capsule::connection()->getDoctrineSchemaManager();
        $columns = $conn->listTableColumns($table);

        return array_map(function ($column) { return $column->getName(); }, $columns);
    }

    /**
     * Sets up the internal columns.
     *
     * @param array $columns
     */
    protected function fillColumns($columns, $all)
    {
        $this->columns = [];
        $table = $this->isJoin() ? $this->select->getModel()->getTable() . '.' : '';

        foreach ($columns as $column) {
            $col = in_array($column, $all) && ! Str::isDot($column) ? $table . $column : $column;

            array_push($this->columns, $col);
        }
    }

    /**
     * Checks if select has joins.
     *
     * @return bool
     */
    protected function isJoin()
    {
        return $this->select->getQuery()->joins ? true : false;
    }

    /**
     * Adds the primary key to the select if it's not selected.
     */
    private function addPrimaryKey()
    {
        $primary = $this->select->getModel()->getKeyName();

        $table = $this->isJoin() ? $this->select->getModel()->getTable() . '.' : '';

        if (! in_array($table . $primary, $this->columns)) {
            $this->select->addSelect($table . $primary);
        }
    }

    /**
     * Get all filter set by filter set functions.
     *
     * @return \Crudix\Filter []
     */
    private function getSetFilters()
    {
        $filters = [];

        foreach ($this->filters as $name) {
            if ($this->hasFilterMethod($name)) {
                array_push($filters, $this->getFilterFromMethod($name));
            }
        }

        return $filters;
    }

    /**
     * Get a filter and set it's name if not set.
     *
     * @param string $name
     * @return \Crudix\Filter
     */
    private function getFilterFromMethod($name)
    {
        $method = $this->getFilterMethodName($name);
        $filter = $this->getFilterMethod($method);

        if (! $filter->name) {
            $filter->setName(Str::getInBetweenString('get', 'Filter', $method));
        }

        return $filter;
    }

    /**
     * Get a filter from filter set function.
     *
     * @param string $method
     * @return \Crudix\Filter
     */
    private function getFilterMethod($method)
    {
        return call_user_func([$this, $method]);
    }

    /**
     * Check if class function is in filter set format.
     *
     * @param string $name
     * @return bool
     */
    private function hasFilterMethod($name)
    {
        $method = $this->getFilterMethodName($name);

        return method_exists($this, $method) && $this->getFilterMethod($method) instanceof Filter;
    }

    /**
     * Get the filter method name;
     *
     * @param $name
     * @return string
     */
    private function getFilterMethodName($name)
    {
        return 'get' . $name . 'Filter';
    }
}
