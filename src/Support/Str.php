<?php

namespace Crudix\Support;

class Str
{
    /**
     * Convert a value to camel case.
     *
     * @param  string  $value
     * @return string
     */
    public static function readable($value)
    {
        $value = static::camel($value);

        return ucwords(preg_replace('/(?!^)[A-Z]{2,}(?=[A-Z][a-z])|[A-Z][a-z]|[0-9]{1,}/', ' $0', $value));
    }

    /**
     * Splits string by dot.
     *
     * @param  string  $value
     * @return array
     */
    public static function dot($value)
    {
        return explode('.', $value);
    }

    /**
     * Check if string is in dot notation.
     *
     * @param  string  $value
     * @return string
     */
    public static function isDot($value)
    {
        return strpos($value, '.') !== FALSE;
    }

    /**
     * Convert a value to camel case.
     *
     * @param  string  $value
     * @return string
     */
    public static function camel($value)
    {
        return lcfirst(static::studly($value));
    }

    /**
     * Convert a value to studly caps case.
     *
     * @param  string  $value
     * @return string
     */
    public static function studly($value)
    {
        $value = ucwords(str_replace(array('.','-', '_'), ' ', $value));

        return str_replace(' ', '', $value);
    }

    /**
     * Get string between 2 delimiters.
     *
     * @param string $start
     * @param string $end
     * @param string $str
     * @return string mixed
     */
    public static function getInBetweenString($start, $end, $str)
    {
        $matches = [];

        preg_match("/$start([a-zA-Z0-9_]*)$end/", $str, $matches);

        return self::camel(isset($matches[1]) ? $matches[1] : $str);
    }

    /**
     * Gets all but last words from dot notation.
     *
     * @param string $value
     * @return string
     */
    public static function firstDot($value)
    {
        $values = explode('.', $value);

        return count($values) ? $values[0] : $value;
    }

    /**
     * Gets the last word from dot notation.
     *
     * @param string $value
     * @return string
     */
    public static function lastDot($value)
    {
        $values = explode('.', $value);

        return $values[count($values) - 1];
    }
}
