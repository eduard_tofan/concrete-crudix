<?php

namespace Crudix\Support;

use Concrete\Core\Form\Service\Form;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;

class Html
{
    /**
     * The base input types.
     *
     * @var array
     */
    protected static $types = [
        'text', 'number', 'select',
        'selectMultiple', 'checkbox', 'search'
    ];

    /**
     * Generates a text input.
     *
     * @param $params \stdClass
     * @return string
     */
    protected static function textInput($params)
    {
        $input = self::label($params);

        return $input . self::generalInput($params, 'text');
    }

    /**
     * Generates a number input.
     *
     * @param $params \stdClass
     * @return string
     */
    protected static function numberInput($params)
    {
        $input = self::label($params);

        return $input . self::generalInput($params, 'text');
    }

    /**
     * Generates a select input.
     *
     * @param $params \stdClass
     * @return string
     */
    protected static function selectInput($params)
    {
        $input = self::label($params);

        return $input . self::generalInput($params, 'select');
    }

    /**
     * Generates a multiple select input.
     *
     * @param $params \stdClass
     * @return string
     */
    protected static function selectMultipleInput($params)
    {
        $input = self::label($params);
        $params->miscFields['class'] = 'select2-select';

        return $input . self::generalInput($params, 'selectMultiple');
    }

    /**
     * Generates a checkbox input.
     *
     * @param $params \stdClass
     * @return string
     */
    protected static function checkboxInput($params)
    {
        $input = self::label($params);

        return $input . self::generalInput($params, 'checkbox');
    }

    /**
     * Generates a search input.
     *
     * @param $params \stdClass
     * @return string
     */
    protected static function searchInput($params)
    {
        $input = self::label($params);
        $input .= '<i class="fa fa-search"></i>';

        return $input . self::generalInput($params, 'search');
    }

    /**
     * Build label if @label params is set.
     *
     * @param $params \stdClass
     * @return string
     */
    protected static function label($params)
    {
        $form = new Form();

        return $params->label ? $form->label($params->name, $params->label, $params->miscFieldsLabel) : '';
    }

    /**
     * Builds an input based on the @type.
     *
     * @param $params \stdClass
     * @param $type string
     * @return string
     */
    protected static function generalInput($params, $type)
    {
        $form = new Form();
        $selects = ['select', 'selectMultiple'];

        if (in_array($type, $selects)) {
            return $form->$type($params->name, $params->options, $params->value, $params->miscFields);
        }

        if ($type == 'checkbox') {
            $checked = $params->value ? true : false;
            return $form->$type($params->name, $params->value, $checked, $params->miscFields);
        }

        return $form->$type($params->name, $params->value, $params->miscFields);
    }

    /**
     * Builds input based on type
     *
     * @param string $type
     * @param array $args
     * @return mixed
     */
    public static function input($type, $args)
    {
        if(! in_array($type, self::$types)) {
            throw new InvalidArgumentException('Type ' . $type . ' is not a valid type.');
        }

        $method = $type . 'Input';

        return call_user_func("self::$method", static::buildArgs($args));
    }

    /**
     * Builds the functions arguments.
     *
     * @param array $args
     * @return \stdClass
     */
    private static function buildArgs($args)
    {
        $argv = new \stdClass();
        $keys = [
            'name' => '', 'value' => '', 'miscFields' => [],
            'options' => [], 'label' => '', 'miscFieldsLabel' => []
        ];

        foreach ($keys as $key => $default) {
            $argv->$key = isset($args[$key]) ? $args[$key] : $default;
        }

        return $argv;
    }
}
