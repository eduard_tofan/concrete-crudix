<?php

namespace Crudix;

use Crudix\Support\Str;
use Illuminate\Support\Collection;

class Response
{
    /**
     * Crud engine instance.
     *
     * @var \Crudix\Engine
     */
    private $engine;

    /**
     * The response data array.
     *
     * @var array
     */
    protected $items = [];

    /**
     * The response column ordering.
     *
     * @var array
     */
    protected $columns = [];

    /**
     * The response active filters.
     *
     * @var array
     */
    protected $fields = [];

    /**
     * The response pagination.
     *
     * @var array
     */
    protected $paginationTemplate = [];

    /**
     * Response constructor.
     * @param \Crudix\Engine $engine
     */
    public function __construct(Engine $engine)
    {
        $this->engine = $engine;
    }

    /**
     * Sets the current active advanced filters.
     *
     * @param array $filters
     */
    public function setFilters($filters)
    {
        $this->fields = $filters;
    }

    /**
     * Sets the column ordering response.
     *
     * @param array $columns
     */
    public function setOrder($columns)
    {
        $this->columns = $columns;
    }

    /**
     * Sets the pagination template response.
     *
     * @param array $pagination
     */
    public function setPagination($pagination)
    {
        $this->paginationTemplate = $pagination;
    }

    /**
     * Builds the response data from the result set.
     *
     * @param $results \Illuminate\Database\Eloquent\Collection;
     */
    public function build($results)
    {
        foreach ($results as $row) {
            $item = $this->buildRow($row);

            array_push($this->items,  $item);
        }
    }

    /**
     * Builds up a result set row in response format.
     *
     * @param \Illuminate\Database\Eloquent\Model $row
     * @return \stdClass
     */
    protected function buildRow($row)
    {
        $item = new \stdClass();

        $item->primary = $row->getKey();

        $columns = $this->buildRowData($this->engine->getColumns(), $row);
        $relationColumns = $this->buildRowData($this->engine->getRelationColumns(), $row);
        $pseudoColumns = $this->buildRowData($this->engine->getPseudoColumns(), $row);

        $item->columns = array_merge($columns, $relationColumns, $pseudoColumns);

        return $item;
    }

    /**
     * Builds up a result set row columns data.
     *
     * @param array $columns
     * @param \Illuminate\Database\Eloquent\Model $row
     * @return array
     */
    protected function buildRowData($columns, $row)
    {
        $cols = [];
        $pseudoMapping = $this->engine->getPseudoColumnMapping();

        foreach ($columns as $column) {
            $col = new \stdClass();

            $col->key = $column;
            $col->value = $this->getColumnValue($column, $row, $pseudoMapping);

            array_push($cols, $col);
        }

        return $cols;
    }

    /**
     * Gets the row's column value.
     *
     * @param string $column
     * @param \Illuminate\Database\Eloquent\Model $row
     * @param array $pseudoMapping
     * @return string
     */
    protected function getColumnValue($column, $row, $pseudoMapping)
    {
        if (in_array($column, $this->engine->getRelationColumns())) {
            $value = $this->getRelationAttribute($column, $row, $pseudoMapping);
        } else {
            $value = $this->getAttribute(Str::lastDot($column), $row, $pseudoMapping);
        }

        $value = $this->wrap($column, $value, $row);

        //default get unique values from array
        $value = is_array($value) ? implode(',', array_unique(array_flatten($value))) : $value;

        return $value;
    }

    /**
     * Gets the row's column value.
     *
     * @param string $column
     * @param \Illuminate\Database\Eloquent\Model $row
     * @param array $pseudoMapping
     * @return string
     */
    protected function getRelationAttribute($column, $row, $pseudoMapping)
    {
        if (in_array($column, $this->engine->getPseudoColumns())) {
            if (! isset($pseudoMapping[$column])) {
                return $column;
            }

            $column = $pseudoMapping[$column];
        }

        return $this->getNestedValue($column, $row);
    }

    /**
     * Get the eloquent relation value.
     *
     * @param string $column
     * @param \Illuminate\Database\Eloquent\Model $model
     * @return array|string
     */
    protected function getNestedValue($column, $model)
    {
        $layers = Str::dot($column);

        foreach ($layers as $layer) {
            $model = $this->getNested($model, $layer);
        }

       return $model;
    }

    /**
     * Gets the nested property of object or collection.
     *
     * @param $object
     * @param  $property
     * @return array
     */
    protected function getNested($object, $property)
    {
        $result = [];

        if ($object instanceof Collection) {
            foreach ($object as $obj) {
                $result[] = object_get($obj, $property);
            }
        } else if (is_array($object)) {
            foreach ($object as $obj) {
                $result[] = $this->getNested($obj, $property);
            }
        } else {
            $result = object_get($object, $property);
        }

        return $result;
    }

    /**
     * Gets the row's column value.
     *
     * @param string $column
     * @param \Illuminate\Database\Eloquent\Model $row
     * @param array $pseudoMapping
     * @return string
     */
    protected function getAttribute($column, $row, $pseudoMapping)
    {
        if (in_array($column, $this->engine->getPseudoColumns())) {
            return isset($pseudoMapping[$column]) ? $row->getAttribute($pseudoMapping[$column]) : $column;
        }

        return $row->getAttribute($column);
    }

    /**
     * Wraps the value if necessary.
     *
     * @param string $column
     * @param string|array $data
     * @param \Illuminate\Database\Eloquent\Model $model
     * @return string
     */
    protected function wrap($column, $data, $model)
    {
        $method = $this->getWrapperMethodName($column);

        return  $this->hasWrap($column) ? call_user_func([$this->engine, $method], $data, $model) : $data;
    }

    /**
     * Checks if column has wrapper.
     *
     * @param string $column
     * @return bool
     */
    private function hasWrap($column)
    {
        return method_exists($this->engine, $this->getWrapperMethodName($column));
    }

    /**
     * Gets the wrap method format.
     *
     * @param string $column
     * @return bool
     */
    private function getWrapperMethodName($column)
    {
        return 'get' . $column . 'Wrapper';
    }

    /**
     * Serialise object to array.
     *
     * @return array
     */
    public function toArray()
    {
        $data = get_object_vars($this);

        unset($data['engine']);

        return $data;
    }

    /**
     * Serialise object to json.
     *
     * @return array
     */
    public function toJson()
    {
        return json_encode($this->toArray());

    }
}
