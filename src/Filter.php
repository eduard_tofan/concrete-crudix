<?php

namespace Crudix;

use Crudix\Support\Html;
use Crudix\Support\Str;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;

/**
 * @method $this alias(string $alias)
 * @method $this value(mixed $value)
 * @method $this label(string $label)
 * @method $this advanced(bool $advanced)
 * @method $this options(array $options)
 * @method $this fields(array $fields)
 * @method $this closure(\Closure $closure)
 * @method $this miscFields(array $miscFields)
 * @method $this miscFieldsLabel(array $miscFieldsLabel)
 */
class Filter
{
    /**
     * Filter name.
     *
     * @var string
     */
    protected $name = '';

    /**
     * Advanced filter display name.
     *
     * @var string
     */
    protected $alias = '';

    /**
     * Filter type [search, select, selectMultiple,
     * checkbox, text, number].
     *
     * @var string
     */
    protected $type;

    /**
     * Filter value.
     *
     * @var string|array
     */
    protected $value = '';

    /**
     * Filter input label name.
     *
     * @var string
     */
    protected $label = '';

    /**
     * Filter in advanced section.
     *
     * @var bool
     */
    protected $advanced = true;

    /**
     * Filter options in case os select and checkbox.
     *
     * @var array
     */
    protected $options = [];

    /**
     * Filter affected fields.
     *
     * @var string
     */
    protected $fields = [];

    /**
     * Filter html markup.
     *
     * @var string
     */
    protected $html;

    /**
     * Filter input html attributes.
     *
     * @var array
     */
    protected $miscFields = [];

    /**
     * Filter label html attributes.
     *
     * @var array
     */
    protected $miscFieldsLabel = [];

    /**
     * Closure function for advanced filters.
     *
     * @var \Closure
     */
    protected $closure = null;

    /**
     * Filter constructor.
     *
     * @param array $params
     */
    public function __construct($params = [])
    {
        $this->fill($params);
        $this->buildHtml();
    }

    /**
     * Magic access getter.
     *
     * @param $name string
     * @return mixed
     */
    public function __get($name)
    {
        if (! property_exists(get_class($this), $name)) {
            throw new InvalidArgumentException('Undefined property via __get(): ' . $name);
        }

        return $this->$name;
    }

    /**
     * @param array $params
     * @return static
     */
    public static function make($params)
    {
        return new static($params);
    }

    /**
     * Adds the html markup to the filter.
     *
     */
    public function buildHtml()
    {
        $this->html = Html::input($this->type, $this->toArray());
    }

    /**
     * Serialise object to array.
     *
     * @return array
     */
    public function toArray()
    {
        $data = get_object_vars($this);
        unset($data['closure']);

        return $data;
    }

    /**
     * Check if filter is in advanced category.
     *
     * @return bool
     */
    public function isAdvanced()
    {
        return $this->advanced;
    }

    /**
     * Check if filter should be used at filtering.
     *
     * @return int
     */
    public function isActive()
    {
        return $this->isValue($this->value);
    }

    /**
     * Check if filter has defined closure.
     *
     * @return bool
     */
    public function hasClosure()
    {
        return $this->closure != null;
    }

    /**
     * Gets the response category for one filter.
     *
     * @return string
     */
    public function getCategory()
    {
        return $this->type == 'search' ? 'search' : ($this->isAdvanced() ? 'advanced' : 'basic');
    }

    /**
     * Value setter.
     *
     * @param string|array $value
     */
    public function setValue($value)
    {
        $this->value = $value;
        $this->buildHtml();
    }

    /**
     * Name setter.
     *
     * @param string|array $name
     */
    public function setName($name)
    {
        $this->name = $name;

        $this->fillDefaultValues();
        $this->buildHtml();
    }

    /**
     * Get filter in field response format.
     *
     * @return array
     */
    public function field()
    {
        return ['field' => $this->name, 'html' => $this->html];
    }

    /**
     * Fills the filter with data.
     *
     * @param array $params
     */
    protected function fill($params)
    {
        foreach ($params as $name => $value) {
            if (! property_exists(get_class($this), $name)) {
                throw new InvalidArgumentException('Unknown parameter ' . $name . ' !');
            }

            $this->$name = $value;
        }

        $this->fillDefaultValues();
    }

    /**
     * Add readable labels and aliases.
     */
    private function fillDefaultValues()
    {
        $this->alias = $this->alias ?: Str::readable($this->name);

        if (! $this->label && $this->type != 'search' && ! $this->isAdvanced()) {
            $this->label = Str::readable($this->name);
        }
    }

    /**
     * Checks if a value exists and has data.
     *
     * @param array|string $value
     *
     * @return int
     */
    protected function isValue($value)
    {
        if (is_array($value)) {
            return count($value);
        }

        return strlen($value);
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $select
     * @param string| array $value
     * @param array $request
     */
    public function runClosure($select, $value, $request)
    {
        call_user_func($this->closure, $select, $value, $request);
    }

    /**
     * Sets the property with the name of the called method.
     *
     * @param string $name
     * @param array $arguments
     * @return \Crudix\Filter
     */
    public function __call($name, $arguments)
    {
        if (! property_exists($this, $name)) {
            throw new InvalidArgumentException('Unknown method ' . $name . ' !');
        }

        if (count($arguments) != 1) {
            throw new InvalidArgumentException('Method ' . $name . ' expects 1 parameter !');
        }

        $this->$name = $arguments[0];

        return $this;
    }
}
