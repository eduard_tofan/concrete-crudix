<?php

namespace Crudix;

use Crudix\Support\Str;

class Sequencer
{
    /**
     * The current selected columns name mapping.
     *
     * @var array
     */
    protected $mapping = [];

    /**
     * The default sorting order.
     *
     * @var string
     */
    protected $defaultOrder = 'asc';

    /**
     * The default sort column.
     * If left unset will sort after first sortable column.
     *
     * @var string
     */
    protected $defaultSortColumn = '';

    /**
     * The model columns which are sortable.
     *
     * @var array
     */
    protected $sortableColumns = ['*'];

    /**
     * Types of filters which are compared with equals operator.
     *
     * @var array
     */
    protected $fixedTypes = ['select', 'selectMultiple', 'text', 'number'];

    /**
     * Sequencer constructor.
     *
     * @param \Crudix\Engine $engine
     */
    public function __construct(Engine $engine)
    {
        $this->engine = $engine;
    }

    /**
     * Set the sequencer params.
     *
     * @param string $defaultOrder
     * @param string $defaultSortColumn
     * @param array $sortableColumns
     * @param array $mapping
     */
    public function setProps($defaultOrder, $defaultSortColumn, $sortableColumns, $mapping)
    {
        $this->defaultOrder = $defaultOrder;
        $this->defaultSortColumn = $defaultSortColumn;
        $this->sortableColumns = $sortableColumns;
        $this->mapping = $mapping;
    }

    /**
     * Build sorting based on request.
     *
     * @return array
     */
    public function build()
    {
        $this->applySort();

        return $this->buildResponseColumns();
    }

    /**
     * Applies the columns sort order.
     */
    protected function applySort()
    {
        if ($this->getOrder()) {
            $this->engine->getSelect()->orderBy($this->getOrderColumn(), $this->getOrder());
        }
    }

    /**
     * Builds the columns response sort data.
     *
     * @return array
     */
    protected function buildResponseColumns()
    {
        $columns = [];

        $cols = array_merge(
            $this->engine->getColumns(),
            $this->engine->getRelationColumns(),
            $this->engine->getPseudoColumns()
        );

        foreach ($cols as $column) {
            $col = $this->buildColumn($column, $this->getColumnMapping($column));

            array_push($columns, $col);
        }

        return $columns;
    }

    /**
     * Builds and returns a column for responseObject.
     *
     * @param string $column
     * @param string $alias
     * @return \stdClass
     */
    protected function buildColumn($column, $alias)
    {
        $col = new \stdClass();

        $col->isColumnSortable = $this->isColumnSortable($column);
        $col->key = $column;
        $col->title = $alias;
        $col->className = $this->getSortingClassName($column);
        $col->direction = $this->getNextSortingOrder($column);

        return $col;
    }

    /**
     * Get sorting css class for column.
     *
     * @param string $column key
     * @return bool|string
     */
    protected function getSortingClassName($column)
    {
        if (! $this->isColumnSortable($column)) {
            return false;
        }

        if ($column == $this->getOrderColumn()) {
            return 'ccm-results-list-active-sort-' . $this->getOrder();
        }

        return false;
    }

    /**
     * Get the next sorting order for column.
     *
     * @param string $column key
     * @return bool|string
     */
    protected function getNextSortingOrder($column)
    {
        if (! $this->isColumnSortable($column)) {
            return false;
        }

        return $this->getOrder() == 'asc' ? 'desc' : 'asc';
    }

    /**
     * Resolve the column title mapping.
     *
     * @param string $column
     * @return string
     */
    protected function getColumnMapping($column)
    {
        $normal = (array_key_exists($column, $this->mapping)) ? $this->mapping[$column] : null;

        $join = (array_key_exists(Str::lastDot($column), $this->mapping)) ? $this->mapping[Str::lastDot($column)] : null;

        return $normal ?: ($join ?: Str::readable($column));
    }

    /**
     * Checks if a certain column is sortable.
     *
     * @param string $column
     * @return bool
     */
    protected function isColumnSortable($column)
    {
        $all = $this->engine->getTableColumns();

        return in_array($column, $this->engine->getColumns()) && in_array(Str::lastDot($column), $all) &&
            (in_array('*', $this->sortableColumns) || in_array($column, $this->sortableColumns));

    }

    /**
     * Gets the current/default sorting order from current request.
     *
     * @return string
     */
    private function getOrder()
    {
        $request = $this->engine->getRequest();

        if (isset($request['direction'])) {
            return $request['direction'];
        }

        return $this->defaultOrder;
    }

    /**
     * Gets the current/default sorting column from current request.
     *
     * @return string
     */
    protected function getOrderColumn()
    {
        $request = $this->engine->getRequest();

        if (isset($request['orderColumn'])) {
            return $request['orderColumn'];
        }

        return $this->getDefaultOrderColumn();
    }

    /**
     * Gets the default sorting column.
     *
     * @return string
     */
    public function getDefaultOrderColumn()
    {
        if ($this->defaultSortColumn) {
            return $this->defaultSortColumn;
        }

        return $this->engine->getColumns()[0];
    }
}
