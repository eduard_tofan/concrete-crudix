<?php defined('C5_EXECUTE') or die("Access Denied.");

    $token = Core::make('helper/validation/token');
    $view->requireAsset('crudix');

    $args = array_merge($args, compact('token'));
?>

<?= $view->element('crudix/table_style', $args, CRUDIX_PACKAGE) ?>

<div class="ccm-dashboard-content-full" data-search="<?= $dataSearch ?>">
    <?= $view->element('crudix/table_structure', $args, CRUDIX_PACKAGE) ?>
    <?= $view->element('crudix/table_search', $args, CRUDIX_PACKAGE) ?>
    <?= $view->element('crudix/table_filters', $args, CRUDIX_PACKAGE) ?>
    <?= $view->element('crudix/table_header', $args, CRUDIX_PACKAGE) ?>
    <?= $view->element('crudix/table_body', $args, CRUDIX_PACKAGE) ?>
    <?= $view->element('crudix/table_pagination', $args, CRUDIX_PACKAGE) ?>
</div>

<?= $view->element('crudix/table_scripts', $args, CRUDIX_PACKAGE) ?>
