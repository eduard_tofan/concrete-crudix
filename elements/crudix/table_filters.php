<?php defined('C5_EXECUTE') or die("Access Denied."); ?>

<script type="text/template" data-template="search-field-row">
    <div class="ccm-search-fields-row">
        <select name="field[]" class="ccm-search-choose-field" data-search-field="<?= $dataSearch ?>">
            <option value=""><?= tc('crudix', 'Choose Field')?></option>
            <?php if ($filters['advanced'] && count($filters['advanced'])) : ?>
                <?php foreach ($filters['advanced'] as $filter): ?>
                    <option value="<?= $filter['name'] ?>"
                        <% if (typeof(field) != 'undefined' && field.field == '<?= $filter['name'] ?>') { print('selected'); } %>
                        data-search-field-url="<?= "$fieldsUrl/{$filter['name']}" ?>">
                        <?= $filter['alias'] ? $filter['alias'] : $filter['name'] ?>
                    </option>
                <?php endforeach; ?>
            <?php endif; ?>
        </select>
        <div class="ccm-search-field-content"><% if (typeof(field) != 'undefined') { print(field.html); } %></div>
        <a data-search-remove="search-field" class="ccm-search-remove-field" href="#">
            <i class="fa fa-minus-circle"></i>
        </a>
    </div>
</script>
