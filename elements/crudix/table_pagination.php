<?php defined('C5_EXECUTE') or die("Access Denied."); ?>

<script type="text/template" data-template="search-results-pagination">
    <?php if ($paginationUrl) : ?>
        <% if (paginationTemplate && paginationTemplate.pages && paginationTemplate.pages.length) { %>
            <ul class="pagination">
                <% if (paginationTemplate.prev) { %>
                    <li class="prev">
                        <a href="<?= $paginationUrl ?>/<%= paginationTemplate.prev %>">&larr;<?= tc('crudix', 'Previous') ?></a>
                    </li>
                <% } else { %>
                    <li class="prev disabled">
                        <span>&larr;<?= tc('crudix', 'Previous') ?></span>
                    </li>
                <% } %>

                <% _.each(paginationTemplate.pages, function (page) { %>
                    <% if (paginationTemplate.page != page) { %>
                        <li>
                            <a href="<?= $paginationUrl ?>/<%= page %>"><%= page %></a>
                        </li>
                    <% } else { %>
                        <li class="active">
                            <span><%= page %><span class="sr-only">(<?= tc('crudix', 'current') ?>)</span>
                        </li>
                    <% } %>
                <% }); %>

                <% if (paginationTemplate.next) { %>
                    <li class="next">
                        <a href="<?= $paginationUrl ?>/<%= paginationTemplate.next %>"><?= tc('crudix', 'Next') ?>&rarr;</a>
                    </li>
                <% } else { %>
                    <li class="prev disabled">
                        <span>&larr;<?= tc('crudix', 'Next') ?></span>
                    </li>
                <% } %>
            </ul>
        <% } %>
    <?php endif; ?>
</script>
