<?php defined('C5_EXECUTE') or die("Access Denied."); ?>

<script type="text/template" data-template="search-results-table-body">
    <% _.each(items, function (item) { %>
        <tr>
            <td style="display: <?= $bulkActions && count($bulkActions) ? '' : 'none' ?>">
                <span class="ccm-search-results-checkbox">
                    <input type="checkbox" class="ccm-flat-checkbox" data-search-checkbox="individual" value="<%= item.primary %>"/>
                </span>
            </td>

            <% _.each(item.columns, function (column) { %>
                <td><%= column.value %></td>
            <% }); %>
        </tr>
    <% }); %>
</script>
