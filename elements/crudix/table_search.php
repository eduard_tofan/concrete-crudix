<?php defined('C5_EXECUTE') or die("Access Denied."); ?>

<script type="text/template" data-template="search-form">
    <form role="form"
          data-search-form="<?= $dataSearch ?>"
          action="<?= $searchUrl ?>" class="form-inline ccm-search-fields"
          style="display: <?= $filters && count($filters) || $bulkActions && count($bulkActions) ? '' : 'none' ?>;">

        <?= $token->output($csrf) ?>

        <div class="ccm-search-fields-row">
            <div class="form-group" style="display: <?= $bulkActions && count($bulkActions) ? '' : 'none' ?>">
                <select data-bulk-action="<?= $dataSearch ?>" disabled class="ccm-search-bulk-action form-control">
                    <option value=""><?= tc('crudix', 'Items Selected') ?></option>
                    <?php if ($bulkActions && count($bulkActions)): ?>
                        <?php foreach ($bulkActions as $action): ?>
                            <option
                                <?php if ($action['attributes'] && count($action['attributes'])): ?>
                                    <?php foreach ($action['attributes'] as $key => $value): ?>
                                        <?= "$key=\"$value\"" ?>
                                    <?php endforeach; ?>
                                <?php endif; ?>>
                                <?= $action['title'] ?>
                            </option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
            </div>

            <div class="form-group" style="display: <?= $searchUrl && $filters['search'] && count($filters['search']) ? '' : 'none' ?>;">
                <div class="ccm-search-main-lookup-field">
                    <?php if ($filters['search'] && count($filters['search'])): ?>
                        <?php foreach ($filters['search'] as $filter): ?>
                            <?php if ($filter['html']): ?>
                                <?= $filter['html'] ?><br />
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <button type="submit" class="ccm-search-field-hidden-submit" tabindex="-1"><?= tc('crudix', 'Search') ?></button>
                </div>
            </div>
            <ul class="ccm-search-form-advanced list-inline" style="display: <?= $searchUrl && $filters['advanced'] && count($filters['advanced']) ? '' : 'none' ?>;">
                <li><a href="#" data-search-toggle="advanced"><?= tc('crudix', 'Advanced Search') ?></a>
            </ul>
        </div>

        <div class="ccm-search-fields-row" style="display: <?= $searchUrl && $filters['basic'] && count($filters['basic']) ? '' : 'none' ?>;">
            <?php if ($filters['basic'] && count($filters['basic'])): ?>
                <?php foreach ($filters['basic'] as $filter): ?>
                    <?php if ($filter['html']): ?>
                        <div class="form-group form-group-full">
                            <?= $filter['html'] ?><br />
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>

        <div class="ccm-search-fields-advanced" style="display: <?= $searchUrl && $filters['advanced'] && count($filters['advanced']) ? '' : 'none' ?>;"></div>
        <div class="ccm-search-fields-submit" style="display: <?= $searchUrl && $filters && count($filters) ? '' : 'none' ?>">
            <button type="submit" class="btn btn-primary pull-right"><?= tc('crudix', 'Search')?></button>
        </div>
    </form>
</script>

