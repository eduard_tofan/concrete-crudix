<?php defined('C5_EXECUTE') or die("Access Denied."); ?>

<script type="text/template" data-template="search-results-table-head">
    <tr>
        <th style="display: <?= $bulkActions && count($bulkActions) ? '' : 'none' ?>">
            <span class="ccm-search-results-checkbox">
                <input type="checkbox" data-search-checkbox="select-all" class="ccm-flat-checkbox">
            </span>
        </th>

        <% _.each(columns, function (column) { %>
            <?php if($sortingUrl): ?>
                <% if (column.isColumnSortable) { %>
                    <th class="<%= column.className %>">
                        <a href="<?= $sortingUrl ?>/<%= column.key %>/<%= column.direction %>"><%= column.title %></a>
                    </th>
                <% } else { %>
                    <th>
                        <span><%= column.title %></span>
                    </th>
                <% } %>
            <?php else: ?>
                <th>
                    <span><%= column.title %></span>
                </th>
            <?php endif; ?>
        <% }); %>
    </tr>
</script>
