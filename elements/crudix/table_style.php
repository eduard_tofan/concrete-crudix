<?php defined('C5_EXECUTE') or die("Access Denied."); ?>

<style type="text/css">
    .crudix > div {
        padding: 0 0 10px;
    }

    .crudix > div h2 {
        border-bottom: 1px solid #ccc
    }

    .crudix .heading {
        margin: 0 30px 10px 0;
    }

    .crudix th, td, span, th > a {
        text-align: center;
    }

    .crudix .ccm-search-results-table td {
        word-break: break-all;
    }

    .crudix .ccm-search-results-table tbody td img {
        max-width: 100%;
    }

    .crudix table.ccm-search-results-table tbody td input[type="checkbox"] {
        z-index: auto;
    }

    .crudix table.ccm-search-results-table tbody td input[type="radio"] {
        z-index: auto;
    }

    .crudix table.ccm-search-results-table span.ccm-search-results-checkbox {
        margin-left: 0;
    }
</style>
