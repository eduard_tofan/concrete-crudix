<?php defined('C5_EXECUTE') or die("Access Denied.");

    $dialogWidth = $dialogWidth ?: 400;
    $dialogHeight = $dialogHeight ?: 400;

    $addButtonOptions = [
        "dialog-modal=\"false\"",
        "dialog-width=\"{$dialogWidth}\"",
        "dialog-height=\"{$dialogHeight}\"",
    ]
?>

<div style="display: <?= $heading ? '' : 'none' ?>;">
    <h2 ><?= $heading ?></h2>
</div>

<div class="<?= ($buttonPosition == 'bottom' ? '' : 'ccm-dashboard-header-buttons') ?>" style="display: <?= $addUrl ? '' : 'none' ?>">
    <?php if ($addUrl) : ?>
        <a href="<?= $addUrl ?>" class="<?= ($dialogTitle ? 'dialog-launch' : '') ?> <?= $heading ? 'heading' : '' ?> btn btn-primary pull-right"
            <?= ($dialogTitle ? explode(' ', $addButtonOptions) : '') ?>
           dialog-title="<?= $dialogTitle ?>">
            <?= strlen($addTitle) ? $addTitle : tc('crudix', 'Add') ?>
        </a>
    <?php endif ?>
</div>

<div data-search-element="wrapper"></div>

<div data-search-element="results">
    <table border="0" cellspacing="0" cellpadding="0" class="ccm-search-results-table">
        <thead></thead>
        <tbody></tbody>
    </table>

    <div class="ccm-search-results-pagination"></div>
</div>
