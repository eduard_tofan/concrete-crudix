<?php defined('C5_EXECUTE') or die("Access Denied."); ?>

<script type="text/javascript">
    var searchObject;

    //fix for advanced search
    if (! String.prototype.format) {
        String.prototype.format = function () {
            var args = arguments;
            var index = -1;
            return this.replace(/{([a-zA-Z]+)}/g, function(match, number) {
                ++index;
                return typeof args[index] != 'undefined' ? args[index] : match;
            });
        };
    }

    $.fn.serializeObject = function () {
        var o = {},
            a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    $(document).ready(function () {
        var dynamic = <?= json_encode($dynamicParameters ?: []) ?>;

        ConcreteAjaxSearch.prototype.setupSearch = function() {
            var cs = this;
            cs.$element.find('[data-search-element=wrapper]').html(cs._templateSearchForm());
            cs.$element.on('submit', 'form[data-search-form]', function() {
                var data = $.extend( $(this).serializeObject(), {crudix_dynamic_request_parameters: dynamic});
                cs.ajaxUpdate($(this).attr('action'), data);
                return false;
            });
        };

        ConcreteAjaxSearch.prototype.setupAdvancedSearch = function () {
            var cs = this;
            cs.$advancedFields = cs.$element.find('div.ccm-search-fields-advanced');

            cs.$element.on('click', 'a[data-search-toggle=advanced]', function () {
                cs.$advancedFields.append(cs._templateAdvancedSearchFieldRow());
                return false;
            });

            cs.$element.on('change', 'select[data-search-field]', function () {
                var $content = $(this).parent().find('.ccm-search-field-content');
                $content.html('');
                var field = $(this).find(':selected').attr('data-search-field-url');
                var token = $("input[name='ccm_token']").val();
                if (field) {
                    cs.ajaxUpdate(field, {
                        ccm_token: token,
                        crudix_dynamic_request_parameters: dynamic
                    }, function (r) {
                        _.each(r.css, function (css) {
                            ccm_addHeaderItem(css, 'CSS');
                        });
                        _.each(r.javascript, function (javascript) {
                            ccm_addHeaderItem(javascript, 'JAVASCRIPT');
                        });
                        $content.html(r.html);
                        $('.select2-select').select2();
                    });
                }
            });

            cs.$element.on('click', 'a[data-search-remove=search-field]', function () {
                var $row = $(this).parent();
                $row.remove();
                return false;
            });
        };

        ConcreteAjaxSearch.prototype.handleSelectedBulkAction = function (value, type, $option, $items) {
            var cs = this,
                itemIDs = [];

            $.each($items, function(i, checkbox) {
                itemIDs.push($(checkbox).val());
            });

            var token = $("input[name='ccm_token']").val();

            if (type == 'dialog') {

                var name = $option.attr('data-bulk-action-name') ? $option.attr('data-bulk-action-name') : value;

                swal({
                        title: $option.attr('data-bulk-action-title'),
                        text: $option.attr('data-bulk-action-message').format(name, $items.size()),
                        type: "error",
                        showCancelButton: true,
                        closeOnConfirm: false,
                        showLoaderOnConfirm: true
                    },
                    function () {
                        $.post($option.attr('data-bulk-action-url'), {
                            'item[]': itemIDs,
                            ccm_token: token,
                            crudix_dynamic_request_parameters: dynamic
                        })
                        .done(function (response) {
                            swal("Success!", "Action executed successfully!", "success");
                        })
                        .fail(function (response) {
                            swal("Whoops!", "Something went wrong!", "error");
                        })
                        .always(function () {
                            if (searchObject) {
                                searchObject.$element.find('select[data-bulk-action]').val('');
                                searchObject.$bulkActions.prop('disabled', true);
                                searchObject.refreshResults();
                            }
                        });
                    }
                );
            }

            if (type == 'ajax') {
                $.concreteAjax({
                    url: $option.attr('data-bulk-action-url'),
                    data: itemIDs,
                    success: function(r) {
                        if (r.message) {
                            ConcreteAlert.notify({
                                'message': r.message,
                                'title': r.title
                            });
                        }
                    }
                });
            }
            cs.publish('SearchBulkActionSelect', {value: value, option: $option, items: $items});
        };

        ConcreteAjaxSearch.prototype.setupPagination = function() {
            var cs = this;

            var token = $("input[name='ccm_token']").val();

            this.$element.on('click', 'ul.pagination a', function() {
                cs.ajaxUpdate($(this).attr('href'), {
                    ccm_token: token,
                    crudix_dynamic_request_parameters: dynamic
                });
                return false;
            });
        };

        ConcreteAjaxSearch.prototype.setupSort = function() {
            var cs = this;

            var token = $("input[name='ccm_token']").val();

            this.$element.on('click', 'thead th a', function() {
                cs.ajaxUpdate($(this).attr('href'), {
                    ccm_token: token,
                    crudix_dynamic_request_parameters: dynamic
                });
                return false;
            });
        };

        // init
        searchObject = $('div[data-search=<?= $dataSearch ?>]').concreteAjaxSearch({
            result: <?= $searchResult ? json_encode($searchResult) : '[]' ?>,
            onUpdateResults: function() {
                $('.dialog-launch').dialog();
                $('.select2-select').select2();
            }
        });
    });
</script>
