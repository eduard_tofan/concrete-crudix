<?php

return [
    'providers' => [
        'crudix' => \Crudix\Provider\CrudixServiceProvider::class
    ],

    'assets' => [
        'sweetalert' => [
            ['css', 'assets/sweetalert/sweetalert.css', ['minify' => true, 'position' => Asset::ASSET_POSITION_FOOTER ], CRUDIX_PACKAGE],
            ['javascript', 'assets/sweetalert/sweetalert.min.js', ['minify' => true], CRUDIX_PACKAGE]
        ],
    ],

    'asset_groups' => [
        'crudix' => [
            [
                ['css', 'sweetalert'],
                ['javascript', 'sweetalert'],
                ['css', 'select2'],
                ['javascript', 'select2'],
                ['javascript-localized', 'select2']
            ]
        ]
    ]
];
