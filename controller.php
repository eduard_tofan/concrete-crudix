<?php

namespace Concrete\Package\Crudix;

use FpsToolkit\Package\Controller as BasePackage;

define('CRUDIX_PACKAGE', 'crudix');

class Controller extends BasePackage
{
    /**
     * Package handle.
     *
     * @var string
     */
	protected $pkgHandle = CRUDIX_PACKAGE;

    /**
     * The current package version.
     *
     * @var string
     */
	protected $pkgVersion = '1.0.0';

    /**
     * Autoload the src folder under
     * the Crudix namespace.
     *
     * @var array
     */
    protected $pkgAutoloaderRegistries = ['src' => '\Crudix'];

    /**
     * Get the package name.
     *
     * @return string
     */
    public function getPackageName()
    {
        return t('Crudix');
    }

    /**
     * Get the package description.
     *
     * @return string
     */
	public function getPackageDescription()
	{
		return t('Laravel model based CRUD tool.');
	}
}
